import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'test--app';
  breakpoint: number = 3
  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 400) ? 6 : 3;
  }
  onResize(event: any) {
    this.breakpoint = (event.target.innerWidth <= 400) ? 6 : 3;
  }
}
