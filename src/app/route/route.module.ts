import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteComponent } from './route.component';

@NgModule({
  declarations: [
    RouteComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class RouteModule { }