import { Component, OnInit } from '@angular/core';
import RouteData from '../../assets/utils/test-data/routes.json';  

@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.scss']
})
export class RouteComponent implements OnInit {
  routes: any = {}
  weekday = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]

  ngOnInit(): void {
    this.routes = RouteData
    this.routes.total = this.routes.loading.length + this.routes.unloading.length
    console.log(RouteData)
  }

  parseDate(dt: any) {
    const d = new Date(dt)
    
    return {
      time: d.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }),
      day: this.weekday[d.getDay()],
      date: `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()}`
    }
  }

}
